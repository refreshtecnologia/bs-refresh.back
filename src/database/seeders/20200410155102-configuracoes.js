'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Configuracoes', [
      {
          Descricao: 'ImpFederal',
          Valor: 2.5,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'CustoFixo',
          Valor: 12,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'CustoMarketing',
          Valor: 3,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'CustoPeD',
          Valor: 3,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'ComissaoRevenda',
          Valor: 50,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'JurosMensal',
          Valor: 2,
          createdAt: created,
          updatedAt: created
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Configuracoes', null, {});
  }
};
