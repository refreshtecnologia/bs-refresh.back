'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UsuariosEmpresas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      UsuarioId: {
        type: Sequelize.INTEGER,
        // references: {
        //   model: "Usuarios",
        //   key: "id"
        // }      
      },
      EmpresaId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        // references: {
        //   model: "Empresas",
        //   key: "id"
        // }      
      },
      Permissao: {
        type: Sequelize.STRING,
        allowNull: false    
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UsuariosEmpresas');
  }
};