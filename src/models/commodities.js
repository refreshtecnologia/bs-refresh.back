const { Model, DataTypes } = require('sequelize');

class Commodities extends Model {
  static init(sequelize) {
    super.init({
      Commodity: DataTypes.STRING,
      Valor: DataTypes.STRING,
      EmpresaId: DataTypes.INTEGER
    }, {
      sequelize
    });
  }
  static associate(models) {
    // associations can be defined here
  };

};
module.exports = Commodities;