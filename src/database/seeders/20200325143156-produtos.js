'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Produtos', [
      {
          Nome: 'Azact',
          Descricao: 'Inseticida e Fungicida',
          MargemLucro: 130,
          EhAzact: true,
          PrecoCustoUnitario: 48,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Azo Plus N30',
          Descricao: 'Nitrogênio 3 fontes +  Ácido Fúlvico',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 3.45,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Black Plus',
          Descricao: 'Indutor de Resistência via solo',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 70,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Boro 10',
          Descricao: 'B 10%',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 7.36,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Cab 2',
          Descricao: 'Ca 10% B 2%',
          MargemLucro: 140,
          EhAzact: false,
          PrecoCustoUnitario: 3.24,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Cirrus',
          Descricao: 'Indutor de Resistência',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 89.34,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Como 10/1',
          Descricao: 'Co 1% Mo 10%',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 55,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Mn 10%',
          Descricao: 'Mn 10%',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 3.98,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Mo 14',
          Descricao: 'Mo 14%',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 34.23,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Nutraza Pellet',
          Descricao: 'Fertilizante orgânico classe A',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 1358.5,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Nutraza Pó',
          Descricao: 'Fertilizante orgânico classe A',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 0,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Orange Fix',
          Descricao: 'D-Limoneno',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 9.03,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'P30',
          Descricao: 'P 30%',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 0,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'PHD I',
          Descricao: 'Adjuvante redutor de pH, Anti-deriva, Anti-espumante, Sequestrante cátions',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 9.32,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'PHD IV',
          Descricao: 'Anti-deriva, Anti-espumante, Sequestrante de cátions',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 8.93,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Platinum',
          Descricao: 'Indutor de Resistência via folha',
          MargemLucro: 120,
          EhAzact: false,
          PrecoCustoUnitario: 70,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Safra Plena',
          Descricao: 'NPK + Micros + Aminoácidos + Algas Marinhas',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 0,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sombrero',
          Descricao: 'Protetor Solar',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 8.23,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte 380',
          Descricao: 'Carbonato de Cálcio Suspensão + Ácido Fúlvico + Extrato de Algas Marinhas',
          MargemLucro: 150,
          EhAzact: false,
          PrecoCustoUnitario: 7.04,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte Gramíneas',
          Descricao: '',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 8.2,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte KS',
          Descricao: 'Enxofre e Potássio Desalojante',
          MargemLucro: 150,
          EhAzact: false,
          PrecoCustoUnitario: 10.83,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte Leguminosas',
          Descricao: '',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 10.85,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte Níquel',
          Descricao: 'Enraizador com Co + Mo + Ni + 15% Extrato de Algas Marinhas',
          MargemLucro: 150,
          EhAzact: false,
          PrecoCustoUnitario: 22.3,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte P',
          Descricao: 'Fósforo de liberação Gradual',
          MargemLucro: 200,
          EhAzact: false,
          PrecoCustoUnitario: 7.43,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte Raiz',
          Descricao: 'Enraizador Extrato de Algas Marinhas',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 40,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Suporte SH',
          Descricao: 'Substância Humicas',
          MargemLucro: 100,
          EhAzact: false,
          PrecoCustoUnitario: 0,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sus Cu',
          Descricao: 'Cu 30%',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 38.45,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sus Lito',
          Descricao: 'Ca 21% Mg 1,3%',
          MargemLucro: 300,
          EhAzact: false,
          PrecoCustoUnitario: 7.95,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sus Mg',
          Descricao: 'Mg 21%',
          MargemLucro: 250,
          EhAzact: false,
          PrecoCustoUnitario: 12.45,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sus Mn',
          Descricao: 'Mn 27%',
          MargemLucro: 250,
          EhAzact: false,
          PrecoCustoUnitario: 13.24,
          createdAt: created,
          updatedAt: created
      },
      {
          Nome: 'Sus Zn',
          Descricao: 'Zn 50%',
          MargemLucro: 250,
          EhAzact: false,
          PrecoCustoUnitario: 23.56,
          createdAt: created,
          updatedAt: created
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Produtos', null, {});

  }
};
