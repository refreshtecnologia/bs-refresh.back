'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'SimuladorVendas',
      'CommodityId', {
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: "Commodities",
          key: "id"
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'SimuladorVendas',
      'CommodityId'
    );
  }
};
