const { Model, DataTypes } = require('sequelize');
var ProdutoSimuladorVendas = require('../models/produtosimuladorvenda');
var Usuarios = require('../models/usuarios');
var Estado = require('../models/estados');
var FormaPgto = require('../models/formaspagamento');
var Empresa = require('../models/empresas');
var Commodity = require('../models/commodities');

class SimuladorVendas extends Model {
  static init(sequelize) {
    super.init({
    UsuarioId: DataTypes.INTEGER,
    EstadoId: DataTypes.INTEGER,
    FormaPagamentoId: DataTypes.INTEGER,
    Frete: DataTypes.FLOAT,
    MargemBruta: DataTypes.FLOAT,
    CustoFixo: DataTypes.FLOAT,
    CustoMarketing: DataTypes.FLOAT,
    ImpostoFederal: DataTypes.FLOAT,
    ICMSFertilizante: DataTypes.FLOAT,
    ICMSAzact: DataTypes.FLOAT,
    ValorVenda: DataTypes.FLOAT,
    ValorCusto: DataTypes.FLOAT,
    MargemLiquida: DataTypes.FLOAT,
    ComissaoVenda: DataTypes.FLOAT,
    ComissaoValor: DataTypes.FLOAT,
    ComissaoAgente: DataTypes.FLOAT,
    PercentualAgente: DataTypes.FLOAT,
    Juros: DataTypes.FLOAT,
    DataEntrega: DataTypes.DATE,
    DataPagamento: DataTypes.DATE,
    Status: DataTypes.INTEGER,
    Cliente: DataTypes.STRING, 
    EmpresaId: DataTypes.INTEGER, 
    CommodityId: DataTypes.INTEGER,
    MargemLiquidaSacas: DataTypes.FLOAT,
    DataAprovacao: DataTypes.DATE,
    FileName: DataTypes.STRING,
    ImpostoCustomizado: DataTypes.FLOAT,
    Observacao : DataTypes.STRING
  }, {
      sequelize
    });
  }
  static associate(models) {
    SimuladorVendas.hasMany(ProdutoSimuladorVendas, {onDelete: 'cascade'});
    SimuladorVendas.belongsTo(Estado);
    SimuladorVendas.belongsTo(Commodity);
    SimuladorVendas.belongsTo(FormaPgto, {foreignKey: 'FormaPagamentoId'});
    SimuladorVendas.belongsTo(Usuarios);
    SimuladorVendas.belongsTo(Empresa);
  };
};

module.exports = SimuladorVendas;