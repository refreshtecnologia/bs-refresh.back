'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Empresas',
      'ImpostoCustomizado', {
        type: Sequelize.FLOAT
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Empresas',
      'ImpostoCustomizado'
    );
  }
};
