'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'SimuladorVendas',
      'DataAprovacao',
      Sequelize.DATEONLY
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'SimuladorVendas',
      'DataAprovacao'
    );
  }
};
