var Simulador = require('../models/simuladorvenda');
var Estado = require('../models/estados');
var FormaPgto = require('../models/formaspagamento');
var Produto = require('../models/produtos');
var Embalagem = require('../models/embalagens');
var ProdutoSimulador = require('../models/produtosimuladorvenda');
var Usuarios = require('../models/usuarios');
var Configuracoes = require('../models/configuracoes');
var Empresas = require('../models/empresas');
var Commodities = require('../models/commodities');
var moment = require('moment');
var email = require('../services/email');
var Enum = require('../config/enum');
var Sequelize = require('sequelize');
var Op = Sequelize.Op;
const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    auth: {
        user: 'contato@nuagro.app',
        pass: 'ribeiroFrenhan'
    }
});


// const verifyRequiredParams = (request) => {
//     request.assert('nome', 'Campo nome é obrigatório').notEmpty();
//     request.assert('email', 'Campo email é obrigatório').notEmpty();

//     var errors = request.validationErrors();
//     if (errors) {
//         error_messages = {
//             error: "true",
//             message: util.inspect(errors)
//         };

//         return false;
//     } else {
//         return true;
//     }
// };

function getSimuladorById(request, response, next) {
    return new Promise((resolve, reject) => {
        Simulador.findOne({
            where: {
                'id': request.params.id
            },
            include: [
                {
                    model: ProdutoSimulador,
                    include: [{
                        model: Produto
                    },
                    {
                        model: Embalagem, as: 'Embalagem'
                    },
                    ]
                },
                {
                    model: Usuarios,
                    include: {
                        model: Empresas
                    }
                },
                {
                    model: FormaPgto
                },
                {
                    model: Estado
                },
                {
                    model: Commodities
                },
                {
                    model: Empresas
                }
            ]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getSimuladorByUser(user) {
    return new Promise((resolve, reject) => {
        Simulador.findAll({
            where: {
                'UsuarioId': user
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produto
                },
                {
                    model: Embalagem, as: 'Embalagem'
                },
                ]
            },
            {
                model: Usuarios,
                include: {
                    model: Empresas
                }
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Empresas
            }
            ]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getSimuladoresByEmpresa(empresaAdm, user) {
    return new Promise((resolve, reject) => {
        Simulador.findAll({
            where: {
                [Op.or]: [
                    {
                        'EmpresaId': empresaAdm
                    },
                    {
                        'UsuarioId': user
                    }
                ]
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produto
                },
                {
                    model: Embalagem, as: 'Embalagem'
                },
                ]
            },
            {
                model: Usuarios,
                include: {
                    model: Empresas
                }
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Empresas
            }
            ]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getEstados(response, next) {
    return new Promise((resolve, reject) => {
        Estado.findAll({})
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

function getFormasPgtos(response, next) {
    return new Promise((resolve, reject) => {
        FormaPgto.findAll({})
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

function getEmbalagens(empresas) {
    return new Promise((resolve, reject) => {
        Embalagem.findAll({
            where: {
                'EmpresaId': empresas
            }
        })
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

function getProdutos(empresas) {
    return new Promise((resolve, reject) => {
        Produto.findAll({
            where: {
                'EmpresaId': empresas
            }
        })
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

function getCommoditiesBarter(empresas) {
    return new Promise((resolve, reject) => {
        Commodities.findAll({
            where: {
                'EmpresaId': empresas
            }
        })
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}


function getConfiguracoes(response, next) {
    return new Promise((resolve, reject) => {
        Configuracoes.findAll({})
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

function getUsuario(id) {
    return new Promise((resolve, reject) => {
        Usuarios.findOne({
            where: {
                'id': id
            },
            include: Empresas
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getEmpresas(empresas) {
    return new Promise((resolve, reject) => {
        Empresas.findAll({
            where: {
                'id': empresas
            }
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function insertProdutoSimuladorVenda(produto, simuladorId) {
    return new Promise((resolve, reject) => {
        ProdutoSimulador.create({
            ProdutoId: produto.ProdutoId,
            SimuladorVendaId: simuladorId,
            EmbalagemId: produto.EmbalagemId,
            Quantidade: produto.Quantidade,
            PrecoVendaUnidade: produto.PrecoVendaUnidade,
            PrecoVendaSacas: produto.PrecoVendaSacas,
            EhBarter: produto.EhBarter,
            createdAt: moment(),
            updatedAt: moment()
        }, {
            include: [Produto]
        }).then(function (retorno) {
            resolve(retorno);
        }).catch(function (err) {
            console.log(err);
        });
    })
}

const dashChartValor = async (empresaAdm, semanal, mensal) => {
    return new Promise((resolve, reject) => {
        var data = moment();
        var mes = data.month() + 1;
        var ano = data.year();
        var semana = data.week();
        console.log(semana);
        Simulador.findAll({
            attributes: [
                'DataAprovacao',
                [Sequelize.fn('sum', Sequelize.col('ValorVenda')), 'Valor'],
            ],
            group: semanal ? Sequelize.fn('day', Sequelize.col('DataAprovacao')) :
                mensal ?
                    Sequelize.fn('month', Sequelize.col('DataAprovacao')) :
                    Sequelize.fn('year', Sequelize.col('DataAprovacao')),
            where:
                Sequelize.and({ 'Status': Enum.Status.Aprovado, 'EmpresaId': empresaAdm },
                    semanal ? Sequelize.where(Sequelize.fn('week', Sequelize.fn('now')), Sequelize.fn('week', Sequelize.col('DataAprovacao'))) :
                        mensal ? Sequelize.where(Sequelize.fn('year', Sequelize.col('DataAprovacao')), ano) : true),
            raw: true,
        }).then(function (retorno) {
            return resolve(retorno);
        })
    })
}

const dashChartContador = async (empresaAdm, semanal, mensal) => {
    return new Promise((resolve, reject) => {
        var data = moment();
        var mes = data.month() + 1;
        var ano = data.year();
        var semana = data.week();
        console.log(semana);
        Simulador.findAll({
            attributes: [
                'DataAprovacao',
                [Sequelize.fn('count', '*'), 'Valor'],
            ],
            group: semanal ? Sequelize.fn('week', Sequelize.col('DataAprovacao')) :
                mensal ?
                    Sequelize.fn('month', Sequelize.col('DataAprovacao')) :
                    Sequelize.fn('year', Sequelize.col('DataAprovacao')),
            where:
                Sequelize.and({ 'Status': Enum.Status.Aprovado, 'EmpresaId': empresaAdm },
                    semanal ? Sequelize.where(Sequelize.fn('week', Sequelize.fn('now')), Sequelize.fn('week', Sequelize.col('DataAprovacao'))) :
                        Sequelize.where(Sequelize.fn('year', Sequelize.col('DataAprovacao')), ano)),
            raw: true,

        }).then(function (retorno) {
            return resolve(retorno)
        })
    })
}

const dashHeader = async (empresaAdm) => {
    return new Promise((resolve, reject) => {
        Simulador.findAll({
            attributes: [
                [Sequelize.fn('count', '*'), 'Quantidade'],
                [Sequelize.fn('sum', Sequelize.col('ValorVenda')), 'Valor'],
                [Sequelize.fn('sum', Sequelize.col('MargemLiquidaSacas')), 'Sacas']
            ],
            where: Sequelize.and({ 'Status': Enum.Status.Aprovado, 'EmpresaId': empresaAdm })
        }).then(function (retorno) {
            return resolve(retorno[0])
        })
    })
}

const ultimosPedidos = async (empresaAdm) => {
    return new Promise((resolve, reject) => {
        Simulador.findAll({
            limit: 5,
            order: [
                ['id', 'DESC'],
            ],
            include: [{
                model: Usuarios,
            }],
            where: Sequelize.and({ 'EmpresaId': empresaAdm })
        }).then(function (retorno) {
            return resolve(retorno)
        })
    })
}

const rankingVenda = async (empresaAdm) => {
    return new Promise((resolve, reject) => {
        Simulador.findAll({
            attributes: [
                'UsuarioId',
                [Sequelize.fn('count', '*'), 'total'],
            ],
            group: 'UsuarioId',

            include: [{
                model: Usuarios,
            }],
            where: Sequelize.and({ 'Status': Enum.Status.Aprovado, 'EmpresaId': empresaAdm })
        }).then(function (retorno) {
            return resolve(retorno)
        })
    })
}





module.exports = {
    async getAllSimuladores(request, response, next) {

        var dados = {
            Simuladores: [],
            Listas: {
                Estados: [],
                FormasPgtos: [],
                Embalagens: [],
                Produtos: [],
                Configuracoes: []
            }
        };

        let user = await getUsuario(request.params.id);

        let empresas = [];
        let empresaAdm = 0;
        user.Empresas.forEach(element => {
            empresas.push(element.UsuariosEmpresas.EmpresaId);
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });

        dados.Simuladores = empresaAdm > 0 ? await getSimuladoresByEmpresa(empresaAdm, user.id)
            : await getSimuladorByUser(user.id);
        dados.Listas.Estados = await getEstados(response, next);
        dados.Listas.FormasPgtos = await getFormasPgtos(response, next);
        dados.Listas.Embalagens = await getEmbalagens(empresas);
        dados.Listas.Produtos = await getProdutos(empresas);
        dados.Listas.Configuracoes = await getConfiguracoes(response, next);
        dados.Listas.Empresas = await getEmpresas(empresas);
        dados.Listas.Commodities = await getCommoditiesBarter(empresas);

        response.send(await dados);
        next();
    },

    async getSimulador(request, response, next) {
        var dados = {
            Simulador: {}
        };

        dados.Simulador = await getSimuladorById(request, response, next);

        response.send(await dados);
        next();
    },


    async addSimulador(request, response, next) {
        Simulador.create({
            UsuarioId: request.body["UsuarioId"],
            Cliente: request.body["Cliente"],
            EstadoId: request.body['EstadoId'],
            FormaPagamentoId: request.body['FormaPagamentoId'],
            Frete: request.body["Frete"] != "" ? request.body['Frete'] : null,
            MargemBruta: request.body['MargemBruta'],
            CustoFixo: request.body['CustoFixo'],
            CustoMarketing: request.body["CustoMarketing"],
            ImpostoFederal: request.body["ImpostoFederal"],
            ICMSFertilizante: request.body["ICMSFertilizante"],
            ICMSAzact: request.body["ICMSAzact"],
            ValorVenda: request.body["ValorVenda"],
            ValorCusto: request.body["ValorCusto"],
            MargemLiquida: request.body["MargemLiquida"],
            ComissaoVenda: request.body["ComissaoVenda"],
            ComissaoValor: request.body["ComissaoValor"],
            ComissaoAgente: request.body["ComissaoAgente"],
            PercentualAgente: request.body["PercentualAgente"] != "" ? request.body["PercentualAgente"] : null,
            Juros: request.body["Juros"],
            DataEntrega: request.body["DataEntrega"] != null ? request.body["DataEntrega"] : null,
            DataPagamento: request.body["DataPagamento"] != null ? request.body["DataPagamento"] : null,
            Status: request.body["Status"],
            ProdutoSimuladorVendas: request.body["ProdutoSimuladorVendas"],
            EmpresaId: request.body["EmpresaId"],
            CommodityId: request.body["CommodityId"],
            MargemLiquidaSacas: request.body["MargemLiquidaSacas"],
            DataAprovacao: request.body["Status"] === Enum.Status.Aprovado ? moment().format('YYYY-MM-DD') : null,
            FileName: request.body["FileName"],
            ImpostoCustomizado: request.body["ImpostoCustomizado"],
            Observacao: request.body["Observacao"],
            createdAt: moment(),
            updatedAt: moment()
        }, {
            include: [ProdutoSimulador]
        }).then(function (retorno) {
            response.send(retorno);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    },

    async updateSimulador(request, response, next) {
        ProdutoSimulador.destroy({
            where: {
                SimuladorVendaId: request.params['id']
            }
        }).then(async function (exclude) {
            await Simulador.update({
                EstadoId: request.body['EstadoId'],
                Cliente: request.body["Cliente"],
                FormaPagamentoId: request.body['FormaPagamentoId'],
                Frete: request.body["Frete"] != "" ? request.body['Frete'] : null,
                MargemBruta: request.body['MargemBruta'],
                CustoFixo: request.body['CustoFixo'],
                CustoMarketing: request.body["CustoMarketing"],
                ImpostoFederal: request.body["ImpostoFederal"],
                ICMSFertilizante: request.body["ICMSFertilizante"],
                ICMSAzact: request.body["ICMSAzact"],
                ValorVenda: request.body["ValorVenda"],
                ValorCusto: request.body["ValorCusto"],
                MargemLiquida: request.body["MargemLiquida"],
                ComissaoVenda: request.body["ComissaoVenda"],
                ComissaoValor: request.body["ComissaoValor"],
                ComissaoAgente: request.body["ComissaoAgente"],
                PercentualAgente: request.body["PercentualAgente"] != "" ? request.body["PercentualAgente"] : null,
                Juros: request.body["Juros"],
                DataEntrega: request.body["DataEntrega"] != null ? moment(request.body["DataEntrega"]) : null,
                DataPagamento: request.body["DataPagamento"] != null ? moment(request.body["DataPagamento"]) : null,
                Status: request.body["Status"],
                ProdutoSimuladorVendas: request.body["ProdutoSimuladorVendas"],
                EmpresaId: request.body["EmpresaId"],
                CommodityId: request.body["CommodityId"] === 0 ? null : request.body["CommodityId"],
                MargemLiquidaSacas: request.body["MargemLiquidaSacas"],
                DataAprovacao: request.body["Status"] === Enum.Status.Aprovado ? moment() : null,
                FileName: request.body["FileName"],
                Observacao: request.body["Observacao"],
                updatedAt: moment(),
                ImpostoCustomizado: request.body["ImpostoCustomizado"]
            },
                {
                    where: { id: request.params.id }
                }).then(async function (retorno) {
                    await request.body["ProdutoSimuladorVendas"].map(produto => insertProdutoSimuladorVenda(produto, request.params.id));
                    var data = {
                        error: "false",
                        message: "Updated Simulador successfully",
                        data: retorno
                    };
                    if (request.body["Status"] === Enum.Status.Aprovado) {
                        var emails = request.body.Empresa.EmailFaturamento.split(';');
                        emails.forEach(element => {
                            email.sendMail(element, "Pedido Aprovado", `Um pedido foi aprovado pelo usuário ${request.body.Usuario.Nome}. `, true, `pedidos/${request.body["EmpresaId"]}/${request.body["FileName"]}`);
                        });
                    }
                    response.send(data);
                    next();
                });
        });
    },

    deleteSimulador(request, response, next) {
        ProdutoSimulador.destroy({
            where: {
                SimuladorVendaId: request.params['id']
            }
        }).then(function (retorno) {
            Simulador.destroy({
                where: {
                    id: request.params['id']
                },
                truncate: { cascade: true, truncate: true },
            }).then(function (retorno) {
                var data = {
                    error: "false",
                    message: "Deleted Simulador successfully",
                    data: retorno
                };

                response.send(data);
                next();
            });
        });
    },

    async addProdutoSimulador(request, response, next) {
        ProdutoSimulador.create({
            ProdutoId: request.body["ProdutoId"],
            SimuladorVendaId: request.body['SimuladorVendaId'],
            EmbalagemId: request.body['EmbalagemId'],
            Quantidade: request.body['Quantidade'],
            PrecoVendaUnidade: request.body['PrecoVendaUnidade'],
            PrecoVendaSacas: request.body['PrecoVendaSacas'],
            EhBarter: request.body['EhBarter'],
            createdAt: moment(),
            updatedAt: moment()
        }, {
            include: [Produto]
        }).then(function (retorno) {
            response.send(retorno);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    },

    async updateProdutoSimulador(request, response, next) {
        ProdutoSimulador.findOne({
            where: {
                'id': request.params.id
            }
        }).then(function (retorno) {
            if (retorno) {
                ProdutoSimulador.create({
                    ProdutoId: request.body["ProdutoId"],
                    EmbalagemId: request.body['EmbalagemId'],
                    Quantidade: request.body['Quantidade'],
                    PrecoVendaUnidade: request.body['PrecoVendaUnidade'],
                    PrecoVendaSacas: request.body['PrecoVendaSacas'],
                    EhBarter: request.body['EhBarter'],
                    updatedAt: moment()
                }).then(function (retorno) {
                    response.send(retorno);
                    next();
                }).catch(function (err) {
                    console.log(err);
                });
            }
        });
    },

    deleteProdutoSimulador(request, response, next) {
        ProdutoSimulador.destroy({
            where: {
                id: request.params['id']
            }
        }).then(function (retorno) {
            var data = {
                error: "false",
                message: "Deleted Produto Simulador successfully",
                data: request.params['id']
            };

            response.send(data);
            next();
        });
    },


    //dash
    async headerDash(request, response, next) {
        let user = await getUsuario(request.params.id);
        let empresas = [];
        let empresaAdm = 0;
        user.Empresas.forEach(element => {
            empresas.push(element.UsuariosEmpresas.EmpresaId);
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });

        var semanaContador = await dashChartContador(empresaAdm, true, false);
        var mesContador = await dashChartContador(empresaAdm, false, true);
        var anoContador = await dashChartContador(empresaAdm, false, false);
        var semanaValor = await dashChartValor(empresaAdm, true, false);
        var mesValor = await dashChartValor(empresaAdm, false, true);
        var anoValor = await dashChartValor(empresaAdm, false, false);

        var data = {
            header: await dashHeader(empresaAdm),
            semanaContador: {
                labels: semanaContador.map(function (semana) {
                    return moment(semana.DataAprovacao).format('dddd');
                }),
                datasets: [
                    {
                        data: semanaContador.map(function (semana) {
                            return semana.Valor;
                        })
                    }
                ]
            },
            mesContador: {
                labels: mesContador.map(function (mes) {
                    return moment(mes.DataAprovacao).format('MMMM');
                }),
                datasets: [
                    {
                        data: mesContador.map(function (mes) {
                            return mes.Valor;
                        })
                    }
                ]
            },
            anoContador: {
                labels: anoContador.map(function (ano) {
                    return moment(ano.DataAprovacao).format('YYYY');
                }),
                datasets: [
                    {
                        data: anoContador.map(function (ano) {
                            return ano.Valor;
                        })
                    }
                ]
            },
            semanaValor: {
                labels: semanaValor.map(function (semana) {
                    return moment(semana.DataAprovacao).format('dddd');
                }),
                datasets: [
                    {
                        data: semanaValor.map(function (semana) {
                            return semana.Valor;
                        })
                    }
                ]
            },
            mesValor: {
                labels: mesValor.map(function (mes) {
                    return moment(mes.DataAprovacao).format('MMMM');
                }),
                datasets: [
                    {
                        data: mesValor.map(function (mes) {
                            return mes.Valor;
                        })
                    }
                ]
            },
            anoValor: {
                labels: anoValor.map(function (ano) {
                    return moment(ano.DataAprovacao).format('YYYY');
                }),
                datasets: [
                    {
                        data: anoValor.map(function (ano) {
                            return ano.Valor;
                        })
                    }
                ]
            },
            ranking: await rankingVenda(empresaAdm),
            pedidos: await ultimosPedidos(empresaAdm)
        }

        response.send(data);
        next();
    },



}
