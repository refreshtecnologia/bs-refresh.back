'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'ProdutoSimuladorVendas',
      'EhBarter',
      Sequelize.BOOLEAN
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'ProdutoSimuladorVendas',
      'EhBarter'
    );
  }
};
