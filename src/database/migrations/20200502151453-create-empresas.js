'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Empresas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Nome: {
        allowNull: false,
        type: Sequelize.STRING
      },
      Telefone: {
        type: Sequelize.STRING
      },
      EmailContato: {
        type: Sequelize.STRING
      },
      EmailFaturamento: {
        type: Sequelize.STRING
      },
      JurosMensal: {
        type: Sequelize.FLOAT
      },
      CustosFixo: {
        type: Sequelize.FLOAT
      },
      CustoMarketing: {
        type: Sequelize.FLOAT
      },
      CustoPeD: {
        type: Sequelize.FLOAT
      },
      ComissaoRepresentante: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Empresas');
  }
};