const { Model, DataTypes } = require('sequelize');
var ProdutoSimuladorVendas = require('../models/produtosimuladorvenda');

class Produtos extends Model {
  static init(sequelize) {
    super.init({
    Descricao: DataTypes.STRING,
    Nome: DataTypes.STRING,
    MargemLucro: DataTypes.FLOAT,
    EhAzact: DataTypes.BOOLEAN,
    PrecoCustoUnitario: DataTypes.FLOAT,   
    Terceirizado: DataTypes.BOOLEAN,   
    EmpresaId: DataTypes.INTEGER   
  }, {
      sequelize
    });
  }
  static associate(models) {
  };

};
module.exports = Produtos;