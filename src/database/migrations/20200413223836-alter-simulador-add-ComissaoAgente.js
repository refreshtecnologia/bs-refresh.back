'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'SimuladorVendas',
      'ComissaoAgente',
      Sequelize.FLOAT
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'SimuladorVendas',
      'ComissaoAgente'
    );
  }
};
