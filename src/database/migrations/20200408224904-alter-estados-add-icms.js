module.exports = {
  up: function(queryInterface, Sequelize) {
    // logic for transforming into the new state
    return queryInterface.addColumn(
      'Estados',
      'ICMSFertilizante',
      Sequelize.FLOAT
    );

  },

  down: function(queryInterface, Sequelize) {
    // logic for reverting the changes
    return queryInterface.removeColumn(
      'Estados',
      'ICMSFertilizante'
    );
  }
}