'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ProdutoSimuladorVendas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      ProdutoId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Produtos",
          key: "id"
        }
      },
      SimuladorVendaId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "SimuladorVendas",
          key: "id"
        }
      },
      EmbalagemId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Embalagens",
          key: "id"
        }
      },
      Quantidade: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      PrecoVendaUnidade: {
        type: Sequelize.FLOAT,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('ProdutoSimuladorVendas');
  }
};