'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'FormasPagamentos',
      'NrParcelas',
      'DiasPagamento'
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'FormasPagamentos',
      'DiasPagamento',
      'NrParcelas'
    );
  }
};
