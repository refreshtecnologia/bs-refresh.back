//somatoria do subtotal de cada pedido na tabela
const Configuracao = {
    "ImpFederal": 1
}

const FormasPgto = {
    "AVista": 1,
    "28Dias": 2,
    "28e56Dias": 3,
    "28e56e84Dias": 4,
    "84Dias": 5,
    "DataEspecifica": 6,
    "Barter": 7
}

function CustoProdutoPedido(product) {
    let valorembalagem = product.Embalagem.PrecoCusto;
    let custoTotal = product.Produto.PrecoCustoUnitario + valorembalagem;
    return parseFloat(custoTotal);
}

module.exports = {
    TotalPedido(products) {
        var total = 0;

        products.forEach(element => {
            total += element.Quantidade * element.PrecoVendaUnidade;
        });
        return parseFloat(total);
    },

    TotalPedidoBarter(products, commodityValor) {
        var total = 0;

        products.forEach(element => {
            total += parseFloat(element.PrecoVendaSacas);
        });
        return parseFloat(total * parseFloat(commodityValor));
    },

    CustoProdutosPedido(products) {
        var custoTotal = 0;

        products.forEach(element => {
            let valorembalagem = element.Embalagem.PrecoCusto;
            custoTotal += element.Quantidade * 
                (element.Produto.PrecoCustoUnitario + valorembalagem);
        });
        return parseFloat(custoTotal);
    },

    MargemBruta(valorPedido, custoPedido) {
        return parseFloat(valorPedido - custoPedido);
    },

    JurosPedido(margemLiquida, formaPgto, dataPagamento, dataEntrega, empresa) {
        //calcular juros composto em cima do valor do pedido
        if(formaPgto){    
            var taxaJurosMes = empresa.JurosMensal;
            var dias = 0;

            if(formaPgto.id === FormasPgto.DataEspecifica)
            {
                var dataAux = new Date(dataPagamento).getTime() - new Date(dataEntrega).getTime();    //Future date - current date
                dias = Math.floor(dataAux / (1000 * 60 * 60 * 24));
            }
            else
                dias = formaPgto.DiasPagamento;

            var valorJuros = taxaJurosMes/30 * dias;

            return parseFloat(margemLiquida * (valorJuros/100));
        }
    },

    ImpostoFederal(valorPedido, configs) {
        var impostoFederal = configs.find((option) => option.id === Configuracao.ImpFederal)
        return parseFloat(valorPedido * (impostoFederal.Valor/100));
    },

    ImpostoCustomizado(valorPedido, empresa) {
        var imposto = empresa.ImpostoCustomizado;
        return parseFloat(valorPedido * (imposto/100));
    },

    ImpostoCustomizado(valorPedido, empresa) {
        var imposto = empresa.ImpostoCustomizado;
        return parseFloat(valorPedido * (imposto/100));
    },

    ICMSFertilizante(products, estado) {
        //se não for azact, multiplicar total do pedido pelo icms do estado da simulação
        var total = 0;

        products.forEach(element => {
            if(!element.Produto.EhAzact)
                total += (element.Quantidade * element.PrecoVendaUnidade) * (estado.ICMSFertilizante/100);
        });

        return parseFloat(total);
    },

    ICMSFertilizanteMargemLiquida(products, estado) {
        //se não for azact, multiplicar total do pedido pelo icms do estado da simulação
        var total = 0;
    
        products.forEach(element => {
            if(!element.Produto.EhAzact)
                total += (element.Quantidade * (element.PrecoVendaUnidade - CustoProdutoPedido(element))) * (estado.ICMSFertilizante/100);
        });
    
        return parseFloat(total);
    },

    ICMSDefensivo(products, estado) {
        //se for azact, multiplicar total do pedido pelo icmsDefensivo do estado da simulação
        var total = 0;

        products.forEach(element => {
            if(element.Produto.EhAzact)
                total += (element.Quantidade * element.PrecoVendaUnidade) * (estado.ICMSDefensivo/100);
        });

        return parseFloat(total);
    },

    ICMSDefensivoMargemLiquida(products, estado) {
        //se for azact, multiplicar total do pedido pelo icmsDefensivo do estado da simulação
        var total = 0;
    
        products.forEach(element => {
            if(element.Produto.EhAzact)
                total += (element.Quantidade * (element.PrecoVendaUnidade - CustoProdutoPedido(element))) * (estado.ICMSDefensivo/100);
        });
    
        return parseFloat(total);
    },

    ICMSFertilizanteBarter(products, estado, commodityValor) {
        //se não for azact, multiplicar total do pedido pelo icms do estado da simulação
        var total = 0;

        products.forEach(element => {
            if(!element.Produto.EhAzact)
                total += (element.PrecoVendaSacas * parseFloat(commodityValor)) * (estado.ICMSFertilizante/100);
        });

        return parseFloat(total);
    },

    ICMSDefensivoBarter(products, estado, commodityValor) {
        //se for azact, multiplicar total do pedido pelo icmsDefensivo do estado da simulação
        var total = 0;

        products.forEach(element => {
            if(element.Produto.EhAzact)
            total += (element.PrecoVendaSacas * parseFloat(commodityValor)) * (estado.ICMSDefensivo/100);
        });

        return parseFloat(total);
    },

    TotalCustoPedido(valorPedido, products, empresa) {
        //somatoria do custo fixo + marketing + p&d + frete + impostos
        let custoFixo = empresa.CustosFixo;
        let vlrPedidoNaoTerceiro = 0;
        products.forEach(element => {
            if(!element.Produto.Terceirizado)
                vlrPedidoNaoTerceiro += (element.Quantidade * element.PrecoVendaUnidade);
        });
        let custoFixoPedido = vlrPedidoNaoTerceiro * (custoFixo/100);

        let custoMarketing = empresa.CustoMarketing;
        let custoMarketingPedido = valorPedido * (custoMarketing/100);

        let custoPeD = empresa.CustoPeD;
        let custoPeDPedido = valorPedido * (custoPeD/100);

        return parseFloat(custoFixoPedido + custoMarketingPedido + custoPeDPedido);
    },

    TotalCustoPedidoBarter(valorPedido, products, empresa, commodityValor) {
        //somatoria do custo fixo + marketing + p&d + frete + impostos
        let custoFixo = empresa.CustosFixo;
        let vlrPedidoNaoTerceiro = 0;
        products.forEach(element => {
            if(!element.Produto.Terceirizado)
                vlrPedidoNaoTerceiro += (element.PrecoVendaSacas * parseFloat(commodityValor));
        });
        let custoFixoPedido = vlrPedidoNaoTerceiro * (custoFixo/100);

        let custoMarketing = empresa.CustoMarketing;
        let custoMarketingPedido = valorPedido * (custoMarketing/100);

        let custoPeD = empresa.CustoPeD;
        let custoPeDPedido = valorPedido * (custoPeD/100);

        return parseFloat(custoFixoPedido + custoMarketingPedido + custoPeDPedido);
    },

    MargemLiquida(margemBruta, totalDescontos, icmsDefensivo, icmsFert, impFederal, impCustomizado, juros, frete) {
        return parseFloat(margemBruta - (totalDescontos + icmsFert + icmsDefensivo + impFederal + impCustomizado + juros + frete));
    },

    ComissaoLiquida(margemLiquida, empresa) {
        let comissaoRepresentante = empresa.ComissaoRepresentante;
        return parseFloat(margemLiquida * (comissaoRepresentante/100));
    },

    ComissaoAgenteRepresentante(comissaoLiquida, comissaoAgente) {
        return parseFloat(comissaoLiquida * (comissaoAgente/100));
    },

    ComissaoRepresentante(comissaoLiquida, comissaoAgente) {
        return parseFloat(comissaoLiquida - comissaoAgente);
    },

    PrecoLista(produto, custoEmbalagem, quantidade) {
        if (!produto) return parseFloat(0);
        let precoLista = (produto.PrecoCustoUnitario + custoEmbalagem) + 
        (produto.PrecoCustoUnitario + custoEmbalagem) * (produto.MargemLucro/100);
        
        return precoLista.toFixed(2);
    },

    ValorConvertidoSacas(produto, custoEmbalagem, quantidade, commodity) {
        if (!produto || !commodity) return parseFloat(0);
        let precoLista = (produto.PrecoCustoUnitario + custoEmbalagem) + 
        (produto.PrecoCustoUnitario + custoEmbalagem) * (produto.MargemLucro/100);
        
        let sacas = (precoLista*parseFloat(quantidade))/parseFloat(commodity);
        return sacas.toFixed(2);
    },

    ImpostoMargemLiquida(margemLiquida, totalImpostos){
        return (margemLiquida - totalImpostos).toFixed(2); 
    }
}