'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Embalagens', [
      {
          Descricao: 'IBC 1000L',
          PrecoCusto: 0,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '20L',
          PrecoCusto: 2,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '20L',
          PrecoCusto: 0,
          EhAzact: true,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '10L',
          PrecoCusto: 2.5,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '4x5L',
          PrecoCusto: 3.5,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      },{
          Descricao: '4x5L',
          PrecoCusto: 0,
          EhAzact: true,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '12x1L',
          PrecoCusto: 5.5,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: '10x1L',
          PrecoCusto: 0,
          EhAzact: true,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'BIG BAG 1T',
          PrecoCusto: 0,
          EhAzact: false,
          createdAt: created,
          updatedAt: created
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Embalagens', null, {});

  }
};
