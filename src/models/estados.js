const { Model, DataTypes } = require('sequelize');

class Estados extends Model {
  static init(sequelize) {
    super.init({
      Descricao: DataTypes.STRING,
      Sigla: DataTypes.STRING,
      ICMSFertilizante: DataTypes.FLOAT,
      ICMSDefensivo: DataTypes.FLOAT
  }, {
      sequelize
    });
  }
  static associate(models) {
    // associations can be defined here
  };

};
module.exports = Estados;