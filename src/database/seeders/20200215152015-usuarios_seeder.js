'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')


module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('usuarios', [
      {
          Auth0Id: 'testeauth0id',
          Nome: 'Hiago',
          Email: 'hiago_frenhan@hotmail.com',
          Telefone: '(16)996047071',
          Estado: 'Minas Gerais',
          Cidade: 'Alfenas',
          Role: '[Administrador]',
          Senha: 'Easy1234',
          Ativo: true,
          createdAt: created,
          updatedAt: created
      },
      {
        Auth0Id: 'testeauth0id2',
        Nome: 'Rodrigo',
        Email: 'rodrigo_a.ribeiro@hotmail.com',
        Telefone: '(11)996735920',
        Estado: 'Minas Gerais',
        Cidade: 'Alfenas',
        Role: '[Administrador]',
        Senha: 'Easy1234',
        Ativo: true,
        createdAt: created,
        updatedAt: created
      },
      {
        Auth0Id: 'testeauth0id3',
        Nome: 'Arthur',
        Email: 'luiz.arthur@lacsa.com.br',
        Telefone: '(16)996047071',
        Estado: 'São Paulo',
        Cidade: 'Ribeirão Preto',
        Role: '[Administrador]',
        Senha: 'Easy1234',
        Ativo: true,
        createdAt: created,
        updatedAt: created
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('usuarios', null, {});
  }
};
