'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Estados', [
      {
          Descricao: 'Acre',
          Sigla: 'AC',
          ICMSFertilizante: 4.9,
          ICMSDefensivo: 2.8,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'Alagoas',
          Sigla: 'AL',
          ICMSFertilizante: 4.9,
          ICMSDefensivo: 2.8,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'Amapá',
          Sigla: 'AP',
          ICMSFertilizante: 4.9,
          ICMSDefensivo: 2.8,
          createdAt: created,
          updatedAt: created
      },
      {
        Descricao: 'Amazonas',
        Sigla: 'AM',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Bahia',
        Sigla: 'BA',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Ceará',
        Sigla: 'CE',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Distrito Federal',
        Sigla: 'DF',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Espírito Santo',
        Sigla: 'ES',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Goiás',
        Sigla: 'GO',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Maranhão',
        Sigla: 'MA',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Mato Grosso',
        Sigla: 'MT',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Mato Grosso do Sul',
        Sigla: 'MS',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Minas Gerais',
        Sigla: 'MG',
        ICMSFertilizante: 8.4,
        ICMSDefensivo: 4.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Pará',
        Sigla: 'PA',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
          Descricao: 'Paraíba',
          Sigla: 'PB',
          ICMSFertilizante: 4.9,
          ICMSDefensivo: 2.8,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'Paraná',
          Sigla: 'PR',
          ICMSFertilizante: 8.4,
          ICMSDefensivo: 4.8,
          createdAt: created,
          updatedAt: created
      },
      {
          Descricao: 'Pernambuco',
          Sigla: 'PE',
          ICMSFertilizante: 4.9,
          ICMSDefensivo: 2.8,
          createdAt: created,
          updatedAt: created
      },
      {
        Descricao: 'Piauí',
        Sigla: 'PI',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Rio de Janeiro',
        Sigla: 'RJ',
        ICMSFertilizante: 8.4,
        ICMSDefensivo: 4.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Rio Grande do Norte',
        Sigla: 'RN',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Rio Grande do Sul',
        Sigla: 'RS',
        ICMSFertilizante: 8.4,
        ICMSDefensivo: 4.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Rondônia',
        Sigla: 'RO',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Roraima',
        Sigla: 'RR',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Santa Catarina',
        Sigla: 'SC',
        ICMSFertilizante: 8.4,
        ICMSDefensivo: 4.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'São Paulo',
        Sigla: 'SP',
        ICMSFertilizante: 0,
        ICMSDefensivo: 0,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Sergipe',
        Sigla: 'SE',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      },
      {
        Descricao: 'Tocantins',
        Sigla: 'TO',
        ICMSFertilizante: 4.9,
        ICMSDefensivo: 2.8,
        createdAt: created,
        updatedAt: created
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('estados', null, {});
  }
};
