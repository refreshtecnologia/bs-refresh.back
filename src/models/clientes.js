const { Model, DataTypes } = require("sequelize");

class Clientes extends Model {
  static init(sequelize) {
    super.init(
      {
        CNPJ: DataTypes.STRING,  
        Abertura: DataTypes.STRING,
        AtividadePrincipal: DataTypes.STRING,
        Bairro: DataTypes.STRING,
        CapitalSocial: DataTypes.STRING,
        Cep: DataTypes.STRING,
        Complemento: DataTypes.STRING,
        Email: DataTypes.STRING,
        Fantasia: DataTypes.STRING,
        Logradouro: DataTypes.STRING,
        Municipio: DataTypes.STRING,
        NaturezaJuridica: DataTypes.STRING,
        Nome: DataTypes.STRING,
        Numero: DataTypes.STRING,
        NumeroInscricao: DataTypes.STRING,
        Porte: DataTypes.STRING,
        Situacao: DataTypes.STRING,
        Telefone: DataTypes.STRING,
        Tipo: DataTypes.STRING,
        UF: DataTypes.STRING,
      },
      {
        sequelize,
      }
    );
  }
  static associate(models) {}
}
module.exports = Clientes;
