'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('FormasPagamentos', [
      {
          Descricao: 'Barter',
          DiasPagamento: 0,
          createdAt: created,
          updatedAt: created
      }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('FormasPagamentos', null, {});

  }
};
