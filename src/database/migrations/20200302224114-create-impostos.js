'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Impostos', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      EstadoId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Estados",
          key: "id"
        }
      },
      Descricao: {
        type: Sequelize.STRING,
      },
      Valor: {
        type: Sequelize.FLOAT,
      },
      EhAzact: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Impostos');
  }
};