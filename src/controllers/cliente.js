var Clientes = require("../models/clientes");

var util = require("../services/util");
var moment = require("moment");
var created = moment().format("YYYY-MM-DD hh:mm:ss");
var Sequelize = require("sequelize");

module.exports = {
  getAllClientes(request, response, next) {
    Clientes.findAll({}).then(function (clientes) {
      response.send(clientes);
      next();
    });
  },
  getClientesByCnpj(request, response, next) {
    Clientes.findOne({
      where: {
        CNPJ: request.params.id,
      },
    }).then(function (cliente) {
      response.send(cliente);
      next();
    });
  },

  getCliente(request, response, next) {
    Clientes.findOne({
      where: {
        id: request.params.id,
      },
    })
      .then(function (cliente) {
        var data = {
          error: "false",
          data: cliente,
        };

        response.send(data);
        next();
      })
      .catch(function (err) {
        console.log(err);
      });
  },

  async addCliente(request, response, next) {
    Clientes.create({
      Abertura: request.body["abertura"],
      Nome: request.body["nome"],
      AtividadePrincipal: request.body["atividade_principal"]["text"],
      Bairro: request.body["bairro"],
      CapitalSocial: request.body["capital_social"],
      Cep: request.body["cep"],
      Complemento: request.body["complemento"],
      Email: request.body["email"],
      Fantasia: request.body["fantasia"],
      Logradouro: request.body["logradouro"],
      Municipio: request.body["municipio"],
      NaturezaJuridica: request.body["natureza_juridica"],
      Numero: request.body["numero"],
      NumeroInscricao: request.body["numero_de_inscricao"],
      Porte: request.body["porte"],
      Situacao: request.body["situacao"],
      Telefone: request.body["telefone"],
      Tipo: request.body["tipo"],
      UF: request.body["uf"],
      CNPJ: request.body["cnpj"],
      createdAt: created,
      updatedAt: created,
    })
      .then(function (cliente) {
        response.send(cliente);
        next();
      })
      .catch(function (err) {
        console.log(err);
      });
  },

  async updateCliente(request, response, next) {
    Clientes.findOne({
      where: {
        id: request.params.id,
      },
    }).then(async function (cliente) {
      if (cliente) {
        cliente
          .update({
            Abertura: request.body["Abertura"],
            Nome: request.body["nome"],
            AtividadePrincipal: request.body["atividade_principal"][0],
            Bairro: request.body["bairro"],
            CapitalSocial: request.body["capital_social"],
            Cep: request.body["cep"],
            Complemento: request.body["complemento"],
            Email: request.body["email"],
            Fantasia: request.body["fantasia"],
            Logradouro: request.body["logradouro"],
            Municipio: request.body["municipio"],
            NaturezaJuridica: request.body["natureza_juridica"],
            Numero: request.body["numero"],
            NumeroInscricao: request.body["numero_de_inscricao"],
            Porte: request.body["porte"],
            Situacao: request.body["situacao"],
            Telefone: request.body["telefone"],
            Tipo: request.body["tipo"],
            UF: request.body["uf"],
            updatedAt: created,
          })
          .then(async function (cliente) {
            var data = {
              error: "false",
              message: "Updated Cliente successfully",
              data: cliente,
            };

            response.send(data);
            next();
          })
          .catch(function (err) {
            console.log(err);
          });
      }
    });
  },

  deleteCliente(request, response, next) {
    Clientes.destroy({
      where: {
        id: request.params["id"],
      },
    })
      .then(function (cliente) {
        var data = {
          error: "false",
          message: "Deleted Cliente successfully",
          data: request.params["id"],
        };

        response.send(data);
        next();
      })
      .catch(function (err) {
        console.log(err);
      });
  },
};
