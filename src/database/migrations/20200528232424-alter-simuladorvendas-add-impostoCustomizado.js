'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'SimuladorVendas',
      'ImpostoCustomizado', {
        type: Sequelize.FLOAT
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'SimuladorVendas',
      'ImpostoCustomizado'
    );
  }
};
