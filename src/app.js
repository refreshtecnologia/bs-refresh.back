var restify = require('restify');
var restifyValidator = require('restify-validator');
var routes = require('./routes');
require('./database');
const corsMiddleware = require('restify-cors-middleware')
 
const cors = corsMiddleware({
  origins: ['https://sistema.nuagro.app', 'http://localhost:3000'],
  allowHeaders:['origin,methods, content-type, accept, Authorization,Control-Allow-Methods,Access-Control-Allow-Headers,Access-Control-Allow-Origin'],
})


var server = restify.createServer();

server.use(restify.plugins.bodyParser());
server.use(restify.plugins.queryParser());
server.use(restifyValidator);
server.pre(cors.preflight)
server.use(cors.actual)
routes.applyRoutes(server);

server.get('/', function (req, res, next) {
  res.send({ hello: 'world' });
  next();
});

server.listen(8000, function() {
    console.log('REST API Server listening at http://localhost:8000');
});