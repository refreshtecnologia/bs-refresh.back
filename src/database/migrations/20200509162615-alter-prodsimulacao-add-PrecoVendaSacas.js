'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'ProdutoSimuladorVendas',
      'PrecoVendaSacas',
      Sequelize.FLOAT
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'ProdutoSimuladorVendas',
      'PrecoVendaSacas'
    );
  }
};
