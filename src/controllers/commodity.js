var Produtos = require('../models/produtos');
var SimuladorVenda = require('../models/simuladorvenda');
var ProdutoSimulador = require('../models/produtosimuladorvenda');
var Embalagem = require('../models/embalagens');
var FormaPgto = require('../models/formaspagamento');
var Estado = require('../models/estados');
var Commodity = require('../models/commodities');
var Empresa = require('../models/empresas');
var util = require('../services/util');
var moment = require('moment')
var Sequelize = require('sequelize');
var Op = Sequelize.Op;

function getSimuladorByCommodity(commodityId) {
    return new Promise((resolve, reject) => {
        SimuladorVenda.findAll({
            where: {
                [Op.and]: [
                    {
                        'CommodityId': commodityId
                    },
                    {
                        'Status': {
                            [Op.ne]: 2
                        } //Não Aprovado
                    }
                ]
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produtos
                },
                {
                    model: Embalagem, as: 'Embalagem'
                }
                ]
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Commodity
            },
            {
                model: Empresa
            }]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

module.exports = {
    getAllCommodity(request, response, next) {
        let empresaAdm = 0;
        request.body.Empresas.forEach(element => {
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });
        Commodity.findAll({
            where: {
                'EmpresaId': empresaAdm
            }
        })
        .then(function (commodity) {

            response.send(commodity);
            next();
        });
    },

    getCommodity(request, response, next) {

        Commodity.findOne({
            where: {
                'id': request.params.id
            }
        }).then(function (commodity) {
            var data = {
                error: "false",
                data: commodity
            };

            response.send(data);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    },


    async addCommodity(request, response, next) {

        Commodity.create({
            Commodity: request.body['Commodity'],
            EmpresaId: request.body['EmpresaId'],
            Valor: request.body['Valor'],
            createdAt: moment(),
            updatedAt: moment()
        }).then(function (commodity) {
            response.send(commodity);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    },

    async updateCommodity(request, response, next) {
        Commodity.findOne({
            where: {
                'id': request.params.id
            }
        }).then(async function (commodity) {
            if (commodity) {
                commodity.update({
                    Commodity: request.body['Commodity'],
                    EmpresaId: request.body['EmpresaId'],
                    Valor: request.body['Valor'],
                    updatedAt: moment(),
                }).then(async function (commodity) {
                    var data = {
                        error: "false",
                        message: "Updated Commodity successfully",
                        data: commodity
                    };

                    let simuladores = await getSimuladorByCommodity(commodity.id);
                    await util.atualizarPedidosRelacionados(simuladores);

                    response.send(data);
                    next();
                }).catch(function (err) {
                    console.log(err);
                });
            }
        });
    },

    deleteCommodity(request, response, next) {
        Commodity.destroy({
            where: {
                id: request.params['id']
            }
        }).then(function (commodity) {
            var data = {
                error: "false",
                message: "Deleted Commodity successfully",
                data: request.params['id']
            };

            response.send(data);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    }
}
