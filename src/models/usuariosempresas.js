const { Model, DataTypes } = require('sequelize');

class UsuariosEmpresas extends Model {
  static init(sequelize) {
    super.init({
      Permissao: DataTypes.STRING
  }, {
      sequelize
    }, { timestamps: false });
  }
  static associate(models) {
  };

};
module.exports = UsuariosEmpresas;
