const { Model, DataTypes } = require('sequelize');

class Configuracoes extends Model {
  static init(sequelize) {
    super.init({
      Descricao: DataTypes.STRING,
      Valor: DataTypes.STRING
  }, {
      sequelize
    });
  }
  static associate(models) {
    // associations can be defined here
  };

};
module.exports = Configuracoes;