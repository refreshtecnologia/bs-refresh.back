const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    host: 'smtp.zoho.com',
    port: 465,
    auth: {
        user: 'contato@nuagro.app',
        pass: 'ribeiroFrenhan'
    }
});
const _from = 'contato@nuagro.app';
const _urlStorage = "https://nuagro-storage.s3-sa-east-1.amazonaws.com/";

module.exports = {
    sendMail(to, subject, text, hasAttachements, filename) {
        let attachments = []
        if (hasAttachements) {
            attachments = [
                {
                    filename: "pedido.pdf",
                    path: `${_urlStorage}${filename}`
                }
            ]
        }
        const message = {
            from: _from,
            to: to,
            subject: subject,
            text: text,
            attachments: attachments
        };
        transport.sendMail(message, function (err, info) {
            if (err) {
                console.log(err)
            } else {
                console.log(info);
            }
        });
    }
}
