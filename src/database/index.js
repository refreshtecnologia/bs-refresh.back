const Sequelize = require('sequelize');
const dbConfig = require('../config/config');

const Configuracao = require('../models/configuracoes');
const Embalagem = require('../models/embalagens');
const Empresa = require('../models/empresas');
const Estado = require('../models/estados');
const FormaPagto = require('../models/formaspagamento');
const Imposto = require('../models/impostos');
const Produto = require('../models/produtos');
const ProdSimulador = require('../models/produtosimuladorvenda');
const Simulador = require('../models/simuladorvenda');
const Usuario = require('../models/usuarios');
const UsuariosEmpresas = require('../models/usuariosempresas');
const Commodity = require('../models/commodities');
const Clientes = require('../models/clientes');

const connection = new Sequelize(dbConfig.development);

Configuracao.init(connection);
Embalagem.init(connection);
Empresa.init(connection);
Estado.init(connection);
FormaPagto.init(connection);
Imposto.init(connection);
Produto.init(connection);
ProdSimulador.init(connection);
Usuario.init(connection);
UsuariosEmpresas.init(connection);
Simulador.init(connection);
Commodity.init(connection);
Clientes.init(connection);

Simulador.associate();
ProdSimulador.associate();
Usuario.associate();
Empresa.associate();
// Empresa.associate();

module.exports = connection;