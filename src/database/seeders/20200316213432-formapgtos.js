'use strict';
var moment = require('moment')
var created = moment().format('YYYY-MM-DD hh:mm:ss')
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('FormasPagamentos', [
      {
          Descricao: 'À vista',
          DiasPagamento: 0,
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
      {
          Descricao: '28 dias',
          DiasPagamento: 28,
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
      {
          Descricao: '28 & 56 dias',
          DiasPagamento: 42,
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
      {
          Descricao: '28, 56 & 84 dias',
          DiasPagamento: 56,
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
      {
          Descricao: '84 dias',
          DiasPagamento: 84,
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
      {
          Descricao: 'Data específica',
          createdAt: created,
          updatedAt: created,
          EmpresaId: 1
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('FormasPagamentos', null, {});

  }
};
