var Produtos = require('../models/produtos');
var SimuladorVenda = require('../models/simuladorvenda');
var ProdutoSimulador = require('../models/produtosimuladorvenda');
var Embalagem = require('../models/embalagens');
var FormaPgto = require('../models/formaspagamento');
var Estado = require('../models/estados');
var Commodities = require('../models/commodities');
var Empresa = require('../models/empresas');
var util = require('../services/util');
var moment = require('moment')
var Sequelize = require('sequelize');
var Op = Sequelize.Op;

function getSimuladorByEmbalagem(empresaId) {
    return new Promise((resolve, reject) => {
        SimuladorVenda.findAll({
            where: {
                [Op.and]: [
                    {
                        'Status': {
                            [Op.ne]: 2
                        }, //Não Aprovado
                    },
                    {
                        'EmpresaId': empresaId
                    }
                ]
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produtos
                },
                {
                    model: Embalagem, as: 'Embalagem'
                }
                ]
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Commodities
            },
            {
                model: Empresa
            }]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

module.exports = {
    getAllEmbalagens(request, response, next) {
        let empresaAdm = 0;
        request.body.Empresas.forEach(element => {
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });

        Embalagem.findAll({
            where: {
                'EmpresaId': empresaAdm
            }
        })
        .then(function (retorno) {
            response.send(retorno);
            next();
        });
    },

    getEmbalagem(request, response, next) {

        Embalagem.findAll({
            where: {
                'id': request.params.id
            }
        }).then(function (retorno) {
            var data = {
                error: "false",
                data: retorno
            };

            response.send(data);
            next();
        });
    },


    async addEmbalagem(request, response, next) {
        Embalagem.create({
            Descricao: request.body["Descricao"],
            PrecoCusto: request.body['PrecoCusto'],
            EhAzact: request.body['EhAzact'],
            EmpresaId: request.body['EmpresaId'],
            createdAt: moment(),
            updatedAt: moment()
            }).then(function (retorno) {
                response.send(retorno);
                next();
            }).catch(function (err) {
                console.log(err);
            });
    },

    async updateEmbalagem(request, response, next) {
        Embalagem.findOne({
            where: {
                'id': request.params.id
            }
        }).then(async function (retorno) {
            if (retorno) {
                retorno.update({
                    Descricao: request.body["Descricao"],
                    PrecoCusto: request.body['PrecoCusto'],
                    EhAzact: request.body['EhAzact'],
                    EmpresaId: request.body['EmpresaId'],
                    updatedAt: moment(),
                }).then(async function (retorno) {
                    var data = {
                        error: "false",
                        message: "Updated Embalagem successfully",
                        data: retorno
                    };

                    let simuladores = await getSimuladorByEmbalagem(retorno.EmpresaId);
                    await util.atualizarPedidosRelacionados(simuladores);

                    response.send(data);
                    next();
                });
            }
        });
    },

    deleteEmbalagem(request, response, next) {
        Embalagem.destroy({
            where: {
                id: request.params['id']
            }
        }).then(function (retorno) {
            var data = {
                error: "false",
                message: "Deleted Embalagem successfully",
                data: retorno
            };

            response.send(data);
            next();
        });
    }
}
