'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Clientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Abertura: {
        type: Sequelize.STRING
      },
      AtividadePrincipal: {
        type: Sequelize.STRING
      },
      Bairro: {
        type: Sequelize.STRING
      },
      CapitalSocial: {
        type: Sequelize.STRING
      },
      Cep: {
        type: Sequelize.STRING
      },
      Complemento: {
        type: Sequelize.STRING
      },
      Email: {
        type: Sequelize.STRING
      },
      Fantasia: {
        type: Sequelize.STRING
      },
      Logradouro: {
        type: Sequelize.STRING
      },
      Municipio: {
        type: Sequelize.STRING
      },
      NaturezaJuridica: {
        type: Sequelize.STRING
      },
      Nome: {
        type: Sequelize.STRING
      },
      Numero: {
        type: Sequelize.STRING
      },
      NumeroInscricao: {
        type: Sequelize.STRING
      },
      Porte: {
        type: Sequelize.STRING
      },
      Situacao: {
        type: Sequelize.STRING
      },
      Telefone: {
        type: Sequelize.STRING
      },
      Tipo: {
        type: Sequelize.STRING
      },
      UF: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Clientes');
  }
};
