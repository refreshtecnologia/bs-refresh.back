var Produtos = require('../models/produtos');
var SimuladorVenda = require('../models/simuladorvenda');
var ProdutoSimulador = require('../models/produtosimuladorvenda');
var Embalagem = require('../models/embalagens');
var FormaPgto = require('../models/formaspagamento');
var Estado = require('../models/estados');
var Commodities = require('../models/commodities');
var Empresa = require('../models/empresas');
var util = require('../services/util');
var moment = require('moment')
var Sequelize = require('sequelize');
var Op = Sequelize.Op;

function getSimuladorByProduto(empresaId) {
    return new Promise((resolve, reject) => {
        SimuladorVenda.findAll({
            where: {
                [Op.and]: [
                    {
                        'Status': {
                            [Op.ne]: 2
                        }, //Não Aprovado
                    },
                    {
                        'EmpresaId': empresaId
                    }
                ]
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produtos
                },
                {
                    model: Embalagem, as: 'Embalagem'
                }
                ]
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Commodities
            },
            {
                model: Empresa
            }]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

module.exports = {
    getAllProdutos(request, response, next) {
        let empresaAdm = 0;
        request.body.Empresas.forEach(element => {
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });

        Produtos.findAll({
            where: {
                'EmpresaId': empresaAdm
            }
        })
        .then(function (produtos) {

            response.send(produtos);
            next();
        });
    },

    getProduto(request, response, next) {

        Produtos.findOne({
            where: {
                'id': request.params.id
            }
        }).then(function (produto) {
            var data = {
                error: "false",
                data: produto
            };

            response.send(data);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    },


    async addProduto(request, response, next) {

        Produtos.create({
            Nome: request.body['Nome'],
            Descricao: request.body['Descricao'],
            MargemLucro: request.body['MargemLucro'],
            EhAzact: request.body['EhAzact'],
            PrecoCustoUnitario: request.body['PrecoCustoUnitario'],
            Terceirizado: request.body['Terceirizado'],
            EmpresaId: request.body['EmpresaId'],
            createdAt: moment(),
            updatedAt: moment()
        }).then(function (produto) {
            response.send(produto);
            next();
        }).catch(function (err) {
                console.log(err);
        });
    },

    async updateProduto(request, response, next) {
        Produtos.findOne({
            where: {
                'id': request.params.id
            }
        }).then(async function (produto) {
            if (produto) {
                produto.update({
                    Nome: request.body['Nome'],
                    Descricao: request.body['Descricao'],
                    MargemLucro: request.body['MargemLucro'],
                    EhAzact: request.body['EhAzact'],
                    PrecoCustoUnitario: request.body['PrecoCustoUnitario'],
                    Terceirizado: request.body['Terceirizado'],
                    EmpresaId: request.body['EmpresaId'],
                    updatedAt: moment(),
                }).then(async function (produto) {
                    var data = {
                        error: "false",
                        message: "Updated Produto successfully",
                        data: produto
                    };
                    let simuladores = await getSimuladorByProduto(produto.EmpresaId);
                    await util.atualizarPedidosRelacionados(simuladores);

                    response.send(data);
                    next();
                }).catch(function (err) {
                    console.log(err);
                });
            }
        });
    },

    deleteProduto(request, response, next) {
        Produtos.destroy({
            where: {
                id: request.params['id']
            }
        }).then(function (produto) {
            var data = {
                error: "false",
                message: "Deleted Produto successfully",
                data: request.params['id']
            };

            response.send(data);
            next();
        }).catch(function (err) {
            console.log(err);
        });
    }
}
