const { Model, DataTypes } = require('sequelize');

class FormasPagamentos extends Model {
  static init(sequelize) {
    super.init({
      Descricao: DataTypes.STRING,
      DiasPagamento: DataTypes.INTEGER
  }, {
      sequelize
    });
  }
  static associate(models) {
    // associations can be defined here
  };

};
module.exports = FormasPagamentos;