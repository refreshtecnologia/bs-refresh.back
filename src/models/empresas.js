const { Model, DataTypes } = require('sequelize');
var Usuarios = require('../models/usuarios');
var Configuracoes = require('../models/configuracoes')

class Empresas extends Model {
  static init(sequelize) {
    super.init({
      Nome: DataTypes.STRING,
      Telefone: DataTypes.STRING,
      EmailContato: DataTypes.STRING,
      EmailFaturamento: DataTypes.STRING,
      JurosMensal: DataTypes.FLOAT,
      CustosFixo: DataTypes.FLOAT,
      CustoMarketing: DataTypes.FLOAT,
      CustoPeD: DataTypes.FLOAT,
      ComissaoRepresentante: DataTypes.FLOAT,
      ImpostoMargemLiquida: DataTypes.BOOLEAN,
      ImpostoCustomizado: DataTypes.FLOAT,
      EstadoId: DataTypes.INTEGER
  }, {
      sequelize
    });
  }
  static associate(models) {
    //Empresas.belongsToMany(Usuarios, { through: 'UsuariosEmpresas' });
    Empresas.hasMany(Configuracoes, {onDelete: 'cascade'});
  };

};
module.exports = Empresas;