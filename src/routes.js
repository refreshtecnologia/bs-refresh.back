var unless = require('express-unless');
var Router = require('restify-router').Router;
var router = new  Router();
var authMiddleware = require('./middleware/auth')
const UserController = require('./controllers/usuario');
const ProdutoController = require('./controllers/produto');
const SimuladorController = require('./controllers/simulador');
const EmpresaController = require('./controllers/empresa');
const EmbalagemController = require('./controllers/embalagem');
const CommodityController = require('./controllers/commodity');
const ClienteController = require('./controllers/cliente');


router.use(authMiddleware.unless({path:[/^\/api\/v1\/usuario\/.*/]}));

//Usuarios
router.post('/api/v1/usuariosByEmpresas',UserController.getAllUsuarios);
router.get('/api/v1/usuario/:email',UserController.getUsuarioByEmail);
router.get('/api/v1/usuarios/:id',UserController.getUsuario);
router.post('/api/v1/usuarios',UserController.addUsuario);
router.put('/api/v1/usuarios/:id',UserController.updateUsuario);
router.del('/api/v1/usuarios/:id',UserController.deleteUsuario);


//Produtos
router.post('/api/v1/produtosByEmpresa',ProdutoController.getAllProdutos);
router.get('/api/v1/produtos/:id',ProdutoController.getProduto);
router.post('/api/v1/produtos',ProdutoController.addProduto);
router.put('/api/v1/produtos/:id',ProdutoController.updateProduto);
router.del('/api/v1/produtos/:id',ProdutoController.deleteProduto);


//Simuladores
router.get('/api/v1/simuladoresByUser/:id',SimuladorController.getAllSimuladores);
router.get('/api/v1/simuladores/:id',SimuladorController.getSimulador);
router.post('/api/v1/simuladores',SimuladorController.addSimulador);
router.put('/api/v1/simuladores/:id',SimuladorController.updateSimulador);
router.del('/api/v1/simuladores/:id',SimuladorController.deleteSimulador);
router.get('/api/v1/Dash/:id',SimuladorController.headerDash);

router.post('/api/v1/produtoSimuladores',SimuladorController.addProdutoSimulador);
router.del('/api/v1/produtoSimuladores/:id',SimuladorController.deleteProdutoSimulador);

//Empresas
router.get('/api/v1/empresas/:id',EmpresaController.getEmpresa);
router.post('/api/v1/empresas',EmpresaController.addEmpresa);
router.put('/api/v1/empresas/:id',EmpresaController.updateEmpresa);
router.del('/api/v1/empresas/:id',EmpresaController.deleteEmpresa);


//Embalagens
router.post('/api/v1/embalagensByEmpresa',EmbalagemController.getAllEmbalagens);
router.post('/api/v1/embalagens',EmbalagemController.addEmbalagem);
router.put('/api/v1/embalagens/:id',EmbalagemController.updateEmbalagem);
router.del('/api/v1/embalagens/:id',EmbalagemController.deleteEmbalagem);


//Commodity
router.post('/api/v1/commodityByEmpresa',CommodityController.getAllCommodity);
router.get('/api/v1/commodity/:id',CommodityController.getCommodity);
router.post('/api/v1/commodity',CommodityController.addCommodity);
router.put('/api/v1/commodity/:id',CommodityController.updateCommodity);
router.del('/api/v1/commodity/:id',CommodityController.deleteCommodity);


//Clientes
router.get('/api/v1/clienteByCNPJ/:id',ClienteController.getClientesByCnpj);
router.get('/api/v1/cliente/:id',ClienteController.getCliente);
router.post('/api/v1/cliente',ClienteController.addCliente);
router.put('/api/v1/cliente/:id',ClienteController.updateCliente);
router.del('/api/v1/cliente/:id',ClienteController.deleteCliente);
module.exports = router