'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'Produtos',
      'Categoria',
      'Terceirizado'
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.renameColumn(
      'Produtos',
      'Categoria',
      'Terceirizado'
    );
  }
};
