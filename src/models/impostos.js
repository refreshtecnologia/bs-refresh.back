const { Model, DataTypes } = require('sequelize');

class Impostos extends Model {
  static init(sequelize) {
    super.init({
      EstadoId: DataTypes.INTEGER,
      Descricao: DataTypes.STRING,
      Valor: DataTypes.FLOAT,
      EhAzact: DataTypes.BOOLEAN
  }, {
      sequelize
    });
  }
  static associate(models) {
  };

};
module.exports = Impostos;