'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SimuladorVendas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      UsuarioId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Usuarios",
          key: "id"
        }
      },
      EstadoId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Estados",
          key: "id"
        }
      },
      FormaPagamentoId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "FormasPagamentos",
          key: "id"
        }
      },
      Frete: {
        type: Sequelize.FLOAT
      },
      MargemBruta: {
        type: Sequelize.FLOAT
      },
      CustoFixo: {
        type: Sequelize.FLOAT
      },
      CustoMarketing: {
        type: Sequelize.FLOAT
      },
      ImpostoFederal: {
        type: Sequelize.FLOAT
      },
      ICMSFertilizante: {
        type: Sequelize.FLOAT
      },
      ICMSAzact: {
        type: Sequelize.FLOAT
      },
      ValorVenda: {
        type: Sequelize.FLOAT
      },
      ValorCusto: {
        type: Sequelize.FLOAT
      },
      MargemLiquida: {
        type: Sequelize.FLOAT
      },
      ComissaoVenda: {
        type: Sequelize.FLOAT
      },
      ComissaoValor: {
        type: Sequelize.FLOAT
      },
      DataEntrega: {
        type: Sequelize.DATE
      },
      DataPagamento: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SimuladorVendas');
  }
};