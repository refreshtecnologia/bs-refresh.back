'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Usuarios', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      Auth0Id:{
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
      },
      Nome:{
          type: Sequelize.STRING,
          allowNull: false
      },
      Email:{
          type: Sequelize.STRING,
          unique: true,
          allowNull: false
      },
      Telefone:{
          type: Sequelize.STRING,
          allowNull: true
      },
      Estado:{
          type: Sequelize.STRING,
          allowNull: true
      },
      Cidade:{
          type: Sequelize.STRING,
          allowNull: true
      },
      Role:{
          type: Sequelize.STRING,
          allowNull: false
      },
      Senha:{
          type: Sequelize.STRING,
          allowNull: false
      },
      Ativo:{
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true
      },
      createdAt:{
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      },
      updatedAt:{
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Usuarios');
  }
};