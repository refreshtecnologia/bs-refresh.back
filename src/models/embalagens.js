const { Model, DataTypes } = require('sequelize');

class Embalagens extends Model {
  static init(sequelize) {
    super.init({
      Descricao: DataTypes.STRING,
      PrecoCusto: DataTypes.FLOAT,
      EhAzact: DataTypes.BOOLEAN, 
      EmpresaId: DataTypes.INTEGER
  }, {
      sequelize
    });
  }
  static associate(models) {
    // associations can be defined here
  };

};
module.exports = Embalagens;