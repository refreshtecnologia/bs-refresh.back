var Produtos = require('../models/produtos');
var SimuladorVenda = require('../models/simuladorvenda');
var ProdutoSimulador = require('../models/produtosimuladorvenda');
var Embalagem = require('../models/embalagens');
var FormaPgto = require('../models/formaspagamento');
var Estado = require('../models/estados');
var Commodities = require('../models/commodities');
var Empresa = require('../models/empresas');
var Configuracoes = require('../models/configuracoes')
var util = require('../services/util');
var moment = require('moment')
var Sequelize = require('sequelize');
var Op = Sequelize.Op;

function getSimuladorByEmpresa(empresaId) {
    return new Promise((resolve, reject) => {
        SimuladorVenda.findAll({
            where: {
                [Op.and]: [
                    {
                        'Status': {
                            [Op.ne]: 2
                        }, //Não Aprovado
                    },
                    {
                        'EmpresaId': empresaId
                    }
                ]
            },
            include: [{
                model: ProdutoSimulador,
                include: [{
                    model: Produtos
                },
                {
                    model: Embalagem, as: 'Embalagem'
                }
                ]
            },
            {
                model: FormaPgto
            },
            {
                model: Estado
            },
            {
                model: Commodities
            },
            {
                model: Empresa
            }]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getEmpresaById(empresaId) {
    return new Promise((resolve, reject) => {
        Empresa.findOne({
            where: {
                'id': empresaId
            },
            include: [{
                model: Configuracoes
            }]
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}

function getEstados(response, next) {
    return new Promise((resolve, reject) => {
        Estado.findAll({})
            .then(function (retorno) {
                resolve(retorno);
            });
    })
}

module.exports = {
    getAllEmpresas(request, response, next) {
        Empresa.findAll({})
            .then(function (retorno) {
                response.send(retorno);
                next();
            });
    },

    async getEmpresa(request, response, next) {

        var dados = {
            Empresa: {},
            Listas: {
                Estados: []
            }
        };

        dados.Empresa = await getEmpresaById(request.params.id);

        dados.Listas.Estados = await getEstados();

        response.send(await dados);
        next();
    },


    async addEmpresa(request, response, next) {
        Empresa.create({
            Nome: request.params["Nome"],
            Telefone: request.params['Telefone'],
            EmailContato: request.params['EmailContato'],
            EmailFaturamento: request.params['EmailFaturamento'],
            JurosMensal: request.params['JurosMensal'],
            CustosFixo: request.params['CustosFixo'],
            CustoMarketing: request.params['CustoMarketing'],
            CustoPeD: request.params['CustoPeD'],
            ComissaoRepresentante: request.params['ComissaoRepresentante'],
            ImpostoMargemLiquida: request.body['ImpostoMargemLiquida'],
            ImpostoCustomizado: request.body['ImpostoCustomizado'],
            EstadoId: request.body['EstadoId'],
            createdAt: moment(),
            updatedAt: moment()
            }).then(function (retorno) {
                response.send(retorno);
                next();
            }).catch(function (err) {
                console.log(err);
            });
    },

    async updateEmpresa(request, response, next) {
        Empresa.findOne({
            where: {
                'id': request.params.id
            }
        }).then(async function (retorno) {
            if (retorno) {
                retorno.update({
                    Nome: request.body["Nome"],
                    Telefone: request.body['Telefone'],
                    EmailContato: request.body['EmailContato'],
                    EmailFaturamento: request.body['EmailFaturamento'],
                    JurosMensal: request.body['JurosMensal'],
                    CustosFixo: request.body['CustosFixo'],
                    CustoMarketing: request.body['CustoMarketing'],
                    CustoPeD: request.body['CustoPeD'],
                    ComissaoRepresentante: request.body['ComissaoRepresentante'],
                    ImpostoMargemLiquida: request.body['ImpostoMargemLiquida'],
                    ImpostoCustomizado: request.body['ImpostoCustomizado'],
                    EstadoId: request.body['EstadoId'],
                    updatedAt: moment(),
                }).then(async function (retorno) {
                    var data = {
                        error: "false",
                        message: "Updated Empresa successfully",
                        data: retorno
                    };

                    let simuladores = await getSimuladorByEmpresa(retorno.id);
                    await util.atualizarPedidosRelacionados(simuladores);

                    response.send(data);
                    next();
                });
            }
        });
    },

    deleteEmpresa(request, response, next) {
        Empresa.destroy({
            where: {
                id: request.params['id']
            }
        }).then(function (retorno) {
            var data = {
                error: "false",
                message: "Deleted Empresa successfully",
                data: retorno
            };

            response.send(data);
            next();
        });
    }
}
