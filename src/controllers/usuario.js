var Usuarios = require('../models/usuarios');
var SimuladorVenda = require('../models/simuladorvenda');
var Empresas = require('../models/empresas');
var UsuariosEmpresas = require('../models/usuariosempresas');
var util = require('util');
var moment = require('moment')
var ManagementClient = require('auth0').ManagementClient;
var AuthenticationClient = require('auth0').AuthenticationClient;
var request = require("request");
var Sequelize = require('sequelize');
var Op = Sequelize.Op;

const verifyRequiredParams = (request) => {
    request.assert('nome', 'Campo nome é obrigatório').notEmpty();
    request.assert('email', 'Campo email é obrigatório').notEmpty();

    var errors = request.validationErrors();
    if (errors) {
        error_messages = {
            error: "true",
            message: util.inspect(errors)
        };

        return false;
    } else {
        return true;
    }
};

async function createUserAuth0(request, token) {
    return new Promise((resolve, reject) => {
        let management = new ManagementClient({
            token: token,
            domain: 'refresh-tecnologia.auth0.com'
        });
        let authentication = new AuthenticationClient({
            domain: 'refresh-tecnologia.auth0.com',
            clientId: 'XyzTGll1dzRKnnBUj0GTmFGPxD4YCq3c',
            clientSecret: 'OTBz_vhWENUDyiVVlsVonOgW4eHFP0cRmK7U8QDFDegrYTBeEXneoCDAoASE6DdK'
        });
        let data = {
            email: request["Email"],
            email_verified: true,
            name: request["Nome"],
            connection: "Username-Password-Authentication",
            password: "Lacsa@1234",
            app_metadata: {
                role: request["Role"]
            }
        }
        management.createUser(data).then(function (response) {
            let data = {
                email: request["Email"],
                connection: "Username-Password-Authentication"
            }

            authentication.requestChangePasswordEmail(data)
            return resolve(response);
        }).catch(function (response) {
            return resolve();
        });
    })
};


async function deleteUserAuth0(id, token) {
    return new Promise((resolve, reject) => {
        let management = new ManagementClient({
            token: token,
            domain: 'refresh-tecnologia.auth0.com'
        });
        let data = {
            id: 'auth0|'+id,
        }
        management.deleteUser(data).then(function (response) {

            return resolve(response);
        }).catch(function (response) {
            return resolve();
        });
    })
};

const createtoken = async (req, authorization) => {
    return new Promise((resolve, reject) => {
        var options = {
            method: 'POST',
            url: 'https://refresh-tecnologia.auth0.com/oauth/token',
            headers: { 'content-type': 'application/json' },
            body: '{"client_id":"XyzTGll1dzRKnnBUj0GTmFGPxD4YCq3c","client_secret":"OTBz_vhWENUDyiVVlsVonOgW4eHFP0cRmK7U8QDFDegrYTBeEXneoCDAoASE6DdK","audience":"https://refresh-tecnologia.auth0.com/api/v2/","grant_type":"client_credentials"}'
        };

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            var response = JSON.parse(body)
            return resolve(response.access_token);
        });
    })
}

function getUsuario(email) {
    return new Promise((resolve, reject) => {
        Usuarios.findOne({
            where: {
                'Email': email
            },
            include: Empresas
        }).then(function (retorno) {
            resolve(retorno);
        });
    })
}
module.exports = {
    getAllUsuarios(request, response, next) {
        let empresaAdm = 0;
        request.body.Empresas.forEach(element => {
            if (element.UsuariosEmpresas.Permissao === "Administrador")
                empresaAdm = element.id;
        });

        Usuarios.findAll({
            include: {
                model: Empresas,
                where: {
                    'id':  empresaAdm
                }
            }
        })
        .then(function (usuarios) {

            response.send(usuarios);
            next();
        });
    },

    getUsuario(request, response, next) {

        Usuarios.findAll({
            where: {
                'id': request.params.id
            },
            include: Empresas
        }).then(function (usuario) {
            var data = {
                error: "false",
                data: usuario
            };

            response.send(data);
            next();
        });
    },

    getUsuarioByEmail(request, response, next) {

        Usuarios.findAll({
            where: {
                'email': request.params.email
            },
            include: Empresas
        }).then(function (usuario) {
            var data = {
                error: "false",
                data: usuario
            };

            response.send(data);
            next();
        });
    },


    async addUsuario(request, response, next) {
        let user = await getUsuario(request.body['Email']);

        if(user){
            Empresas.findOne({
                where: {
                    'id': request.body['EmpresaId']
                }
            }).then(function (empresa) {
                user.addEmpresas(empresa, {through: {Permissao: request.body['Role']}})
                .then(function (result) {
                    response.send(user);
                    next();
                });
            });
        }
        else{
            let token = await createtoken();
            let userAuth0 = await createUserAuth0(request.body, token);

            if (userAuth0)
                Usuarios.create({
                    Auth0Id: userAuth0.identities[0]["user_id"],
                    Nome: request.body['Nome'],
                    Email: request.body['Email'],
                    Telefone: request.body['Telefone'],
                    Estado: request.body['Estado'],
                    Cidade: request.body['Cidade'],
                    Ativo: true,
                    Senha: "[Senha]",
                    Role: request.body['Role'],
                    createdAt: moment(),
                    updatedAt: moment()
                }).then(function (usuario) {
                    Empresas.findOne({
                        where: {
                            'id': request.body['EmpresaId']
                        }
                    }).then(function (empresa) {
                        usuario.addEmpresas(empresa, {through: {Permissao: request.body['Role']}})
                        .then(function (result) {
                            response.send(usuario);
                            next();
                        });
                    });
                }).catch(function (err) {
                    console.log(err);
                });
            else
                response.send(422, "Erro ao criar usuário");
        }
    },

    updateUsuario(request, response, next) {
        Usuarios.findOne({
            where: {
                'id': request.params.id
            }
        }).then(function (usuario) {
            if (usuario) {
                usuario.update({
                    Nome: request.body['Nome'],
                    Telefone: request.body['Telefone'],
                    Estado: request.body['Estado'],
                    Cidade: request.body['Cidade'],
                    Role: request.body['Role'],
                    updatedAt: moment(),
                }).then(async function (usuario) {
                    Empresas.findOne({
                        where: {
                            'id': request.body['EmpresaId']
                        }
                    }).then(async function (empresa) {
                        await usuario.addEmpresas(empresa);
            
                        response.send(usuario);
                        next();
                    });
                }).catch(function (err) {
                    console.log(err);
                });
            }
        });
    },

    async deleteUsuario(request, response, next) {
        Usuarios.findAll({
            where: {
                'id': request.params.id
            }
        }).then(function (usuario) {
            Usuarios.destroy({
                where: {
                    id: request.params['id']
                }
            }).then(async function (del) {
                let token = await createtoken();
                let userAuth0 = await deleteUserAuth0(usuario[0].Auth0Id, token);
                var data = {
                    error: "false",
                    message: "Deleted Usuario successfully",
                    data: usuario[0]
                };

                response.send(data);
                next();
            });
        });
    }
}
