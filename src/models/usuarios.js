const { Model, DataTypes } = require('sequelize');
var Empresas = require('../models/empresas');

class Usuarios extends Model {
  static init(sequelize) {
    super.init({
      Auth0Id: DataTypes.STRING,
      Nome: DataTypes.STRING,
      Email: DataTypes.STRING,
      Telefone: DataTypes.STRING,
      Estado: DataTypes.STRING,
      Cidade: DataTypes.STRING,
      Role: DataTypes.STRING,
      Senha: DataTypes.STRING,
      Ativo:DataTypes.BOOLEAN,
    }, {
      sequelize
    });
  }
  static associate(models) {
    Usuarios.belongsToMany(Empresas, { through: 'UsuariosEmpresas' });
  };

};
module.exports = Usuarios;