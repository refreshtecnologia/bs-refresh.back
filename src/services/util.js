const Math = require('./mathService');
var Configuracoes = require('../models/configuracoes');
var Simulador = require('../models/simuladorvenda');
var moment = require('moment')

async function salvarSimulador(simulador) {
    await Simulador.update({
            MargemBruta: simulador.MargemBruta,
            ImpostoFederal: simulador.ImpostoFederal,
            ImpostoCustomizado: simulador.ImpostoCustomizado,
            ICMSFertilizante: simulador.ICMSFertilizante,
            ICMSAzact: simulador.ICMSAzact,
            ValorVenda: simulador.ValorVenda,
            ValorCusto: simulador.ValorCusto,
            MargemLiquida: simulador.MargemLiquida,
            ComissaoVenda: simulador.ComissaoVenda,
            ComissaoValor: simulador.ComissaoValor,
            ComissaoAgente: simulador.ComissaoAgente,
            Juros: simulador.Juros,
            ProdutoSimuladorVendas: simulador.ProdutoSimuladorVendas,
            MargemLiquidaSacas: simulador.MargemLiquidaSacas,
            updatedAt: moment(),
        },
        {
            where: { id: simulador.id },
        }).then(async function (retorno) {
            return retorno;
        });
}

async function atualizarSimuladores(listaPedidos, configs) {
    return new Promise((resolve, reject) => {
        listaPedidos.forEach(element => {
            let ehBarter = element.FormaPagamentoId === 7;
            let ehImpostoCustomizado = element.Empresa.ImpostoCustomizado;
            let ehImpostoMargemLiquida = element.Empresa.ImpostoMargemLiquida ? true: false;

            if(ehBarter) //Barter
            {
                element.ProdutoSimuladorVendas.forEach(prodSimulador => {
                    prodSimulador.PrecoVendaSacas = Math.ValorConvertidoSacas(prodSimulador.Produto, prodSimulador.Embalagem.PrecoCusto, prodSimulador.Quantidade, element.Commodity.Valor);                        
                });
                element.ValorVenda = Math.TotalPedidoBarter(element.ProdutoSimuladorVendas, element.Commodity.Valor);
            }
            let vlrCustoProdutosPedido = Math.CustoProdutosPedido(element.ProdutoSimuladorVendas);
            element.MargemBruta = Math.MargemBruta(element.ValorVenda, vlrCustoProdutosPedido);
            element.Juros = Math.JurosPedido(element.ValorVenda, element.FormasPagamento, element.DataPagamento, element.DataEntrega, element.Empresa);
            let vlrCustoTotalPedido = ehBarter ? Math.TotalCustoPedidoBarter(element.ValorVenda, element.ProdutoSimuladorVendas, element.Empresa, element.Commodity.Valor) : Math.TotalCustoPedido(element.ValorVenda, element.ProdutoSimuladorVendas, element.Empresa);
            
            //margem líquida
            let margemLiquida = 0.00;
            if(ehImpostoMargemLiquida)
            {
                let margemLiquidaSemImposto = Math.MargemLiquida(element.MargemBruta, 0, 0, 0, 0, 0, element.Juros, element.Frete);
                //impostos
                if(ehImpostoCustomizado)
                {
                    element.ImpostoCustomizado = Math.ImpostoCustomizado(margemLiquidaSemImposto, element.Empresa);
                    margemLiquida = Math.MargemLiquida(margemLiquidaSemImposto, 0, 0, 0, 0, element.ImpostoCustomizado, 0, 0);
                    margemLiquida = Math.MargemLiquida(margemLiquida, vlrCustoTotalPedido, 0, 0, 0, 0, 0, 0);
                }
                else{
                    element.ImpostoFederal = Math.ImpostoFederal(margemLiquidaSemImposto, configs);
                    if(element.EstadoId !== element.Empresa.EstadoId)
                    {
                        element.ICMSFertilizante = ehBarter ? Math.ICMSFertilizanteBarter(element.ProdutoSimuladorVendas, element.Estado, element.Commodity.Valor) : Math.ICMSFertilizanteMargemLiquida(element.ProdutoSimuladorVendas, element.Estado);
                        element.ICMSAzact = ehBarter ? Math.ICMSDefensivoBarter(element.ProdutoSimuladorVendas, element.Estado, element.Commodity.Valor) : Math.ICMSDefensivoMargemLiquida(element.ProdutoSimuladorVendas, element.Estado);
                    }
                    else{
                        element.ICMSFertilizante = 0;
                        element.ICMSAzact = 0;
                    }
                    margemLiquida = Math.MargemLiquida(element.MargemBruta, vlrCustoTotalPedido, element.ICMSAzact, element.ICMSFertilizante, element.ImpostoFederal, 0, element.Juros, element.Frete);
                }
            }
            else{
                //impostos
                if(ehImpostoCustomizado)
                {
                    element.ImpostoCustomizado = Math.ImpostoCustomizado(element.ValorVenda, element.Empresa);
                    margemLiquida = Math.MargemLiquida(element.MargemBruta, vlrCustoTotalPedido, 0, 0, 0, element.ImpostoCustomizado, element.Juros, element.Frete);
                }
                else{
                    element.ImpostoFederal = Math.ImpostoFederal(element.ValorVenda, configs);
                    if(element.EstadoId !== element.Empresa.EstadoId)
                    {
                        element.ICMSFertilizante = ehBarter ? Math.ICMSFertilizanteBarter(element.ProdutoSimuladorVendas, element.Estado, element.Commodity.Valor) : Math.ICMSFertilizante(element.ProdutoSimuladorVendas, element.Estado);
                        element.ICMSAzact = ehBarter ? Math.ICMSDefensivoBarter(element.ProdutoSimuladorVendas, element.Estado, element.Commodity.Valor) : Math.ICMSDefensivo(element.ProdutoSimuladorVendas, element.Estado);
                    }
                    else{
                        element.ICMSFertilizante = 0;
                        element.ICMSAzact = 0;
                    }
                    margemLiquida = Math.MargemLiquida(element.MargemBruta, vlrCustoTotalPedido, element.ICMSAzact, element.ICMSFertilizante, element.ImpostoFederal, 0, element.Juros, element.Frete);
                }    
            }
            element.ComissaoValor = Math.ComissaoLiquida(margemLiquida, element.Empresa);
            element.ComissaoAgente = Math.ComissaoAgenteRepresentante(element.ComissaoValor, element.PercentualAgente);
            element.ComissaoVenda = Math.ComissaoRepresentante(element.ComissaoValor, element.ComissaoAgente);
            element.MargemLiquida = margemLiquida;
            if(element.Commodity)
                element.MargemLiquidaSacas = margemLiquida/parseFloat(element.Commodity.Valor);

            salvarSimulador(element);
        });
    })
}

module.exports = {
    async atualizarPedidosRelacionados(simuladores) {
        Configuracoes.findAll({})
        .then(async function (configs) {
            await atualizarSimuladores(simuladores, configs);
        });
    }
};