'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Empresas',
      'EstadoId', {
        type: Sequelize.INTEGER,
        defaultValue: 25,
        allowNull: false,
        references: {
          model: "Estados",
          key: "id"
        }
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Empresas',
      'EstadoId'
    );
  }
};
