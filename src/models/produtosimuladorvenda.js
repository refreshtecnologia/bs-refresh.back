const { Model, DataTypes } = require('sequelize');
var Produto = require('../models/produtos');
var Embalagem = require('../models/embalagens');

class ProdutoSimuladorVendas extends Model {
  static init(sequelize) {
    super.init({
    ProdutoId: DataTypes.INTEGER,
    SimuladorVendaId: DataTypes.INTEGER,
    EmbalagemId: DataTypes.INTEGER,
    Quantidade: DataTypes.FLOAT,
    PrecoVendaUnidade: DataTypes.FLOAT,
    PrecoVendaSacas: DataTypes.FLOAT,
    EhBarter: DataTypes.BOOLEAN
  }, {
      sequelize
    });
  }
  static associate(models) {
    ProdutoSimuladorVendas.belongsTo(Produto);
    ProdutoSimuladorVendas.belongsTo(Embalagem, {foreignKey: 'EmbalagemId', as: 'Embalagem'});
  };
};
module.exports = ProdutoSimuladorVendas;